#10-22 added functionality so the variables are entered at the beginning of the process. Should I add something to pull
    #the files by enumerator so each indivudual one can be passed to another function?
#10-21 Added cmdletbinding line, supportswildcards to paramter block and -like to the first get-childitem
#10-21 just call the directory - pass the GCI variable and let the gui choose the variables?

function GetFileDirinfo {
[CmdletBinding()] 

Param(
    [SupportsWildcards()][string[]]$path,
    [SupportsWildcards()][string]$ext,
    [SupportsWildcards()][string[]]$pattern,
    $limit=255)

$Path = Read-Host "Please enter path"
$limit = Read-Host "Please enter recurse limit (default 9)"
$ext = Read-Host "Please enter ext pattern to search for (wildcards accepted)"
$pattern = Read-Host "Please enter name pattern to search for (wildcards accepted)"

$F1=Get-ChildItem $path -Recurse -Depth $limit |Where-Object { $_.PSIsContainer -and $_.Name -like "*$pattern*"}

$files = @()
Get-ChildItem -Path $F1.FullName -Filter $ext -Recurse -File | ForEach-Object{ 
$s= $_.FullName.Remove($_.FullName.Length - $_.Extension.Length)
$file= New-Object PSObject -property @{FileName=[System.IO.Path]::GetFileName($_);CreationTime=$_.CreationTime;LastAccessTime=$_.LastAccessTime;LastWriteTime=$_.LastWriteTime;FullName=$_.FullName;Length=$_.Length; path=$s.Remove($s.Length - $_.BaseName.Length - 1)}
$files+=$file
}

$files.GetEnumerator() 

}