*--readme for getfiledirinfo function--*
*--author: Michael Pellas for AppDev-287--*

function to be added to rxssendgridfunctions.ps1?

--
as of 10/25/2021, function pulls only from variables. (original test code pulled from "\\rxs-main\RxSFiles\Testing") 
I have it set up for user input. This can be updated easily. 

does it need to pull from \\rxs-veuropa-fs\Home\vpn_DevFactory\ContractPharmacy\PharmacyBills?

$path variable is self explanatory
$limit limits path variable recursion depth, ex:\\rxs-veuropa-fs\Home\vpn_DevFactory for current set of 2
$ext filters for specific file extensions
$pattern filters for a string pattern. wildcards included.

result array is output via $files variable. each object can be called.